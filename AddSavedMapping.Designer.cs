﻿namespace Portmapper
{
    partial class FormAddSavedMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupdevicebinding = new System.Windows.Forms.GroupBox();
            this.portbindall = new System.Windows.Forms.RadioButton();
            this.portbindselected = new System.Windows.Forms.RadioButton();
            this.boxdescription = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grouplifetime = new System.Windows.Forms.GroupBox();
            this.boxautorenew = new System.Windows.Forms.CheckBox();
            this.boxlifetime = new System.Windows.Forms.NumericUpDown();
            this.lifetimespecify = new System.Windows.Forms.RadioButton();
            this.lifetimeinfinite = new System.Windows.Forms.RadioButton();
            this.btnaddmapping = new System.Windows.Forms.Button();
            this.groupprotocol = new System.Windows.Forms.GroupBox();
            this.protocolboth = new System.Windows.Forms.RadioButton();
            this.protocoludp = new System.Windows.Forms.RadioButton();
            this.protocoltcp = new System.Windows.Forms.RadioButton();
            this.boxprivateport = new System.Windows.Forms.NumericUpDown();
            this.boxpublicport = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupdevicebinding.SuspendLayout();
            this.grouplifetime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boxlifetime)).BeginInit();
            this.groupprotocol.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boxprivateport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxpublicport)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Private Port: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Public Port: ";
            // 
            // groupdevicebinding
            // 
            this.groupdevicebinding.Controls.Add(this.portbindall);
            this.groupdevicebinding.Controls.Add(this.portbindselected);
            this.groupdevicebinding.Location = new System.Drawing.Point(9, 90);
            this.groupdevicebinding.Name = "groupdevicebinding";
            this.groupdevicebinding.Size = new System.Drawing.Size(115, 71);
            this.groupdevicebinding.TabIndex = 4;
            this.groupdevicebinding.TabStop = false;
            this.groupdevicebinding.Text = "Device Binding";
            // 
            // portbindall
            // 
            this.portbindall.AutoSize = true;
            this.portbindall.Location = new System.Drawing.Point(6, 42);
            this.portbindall.Name = "portbindall";
            this.portbindall.Size = new System.Drawing.Size(78, 17);
            this.portbindall.TabIndex = 1;
            this.portbindall.Text = "All Devices";
            this.portbindall.UseVisualStyleBackColor = true;
            // 
            // portbindselected
            // 
            this.portbindselected.AutoSize = true;
            this.portbindselected.Checked = true;
            this.portbindselected.Location = new System.Drawing.Point(6, 19);
            this.portbindselected.Name = "portbindselected";
            this.portbindselected.Size = new System.Drawing.Size(104, 17);
            this.portbindselected.TabIndex = 0;
            this.portbindselected.TabStop = true;
            this.portbindselected.Text = "Selected Device";
            this.portbindselected.UseVisualStyleBackColor = true;
            // 
            // boxdescription
            // 
            this.boxdescription.Location = new System.Drawing.Point(76, 12);
            this.boxdescription.Name = "boxdescription";
            this.boxdescription.Size = new System.Drawing.Size(168, 20);
            this.boxdescription.TabIndex = 6;
            this.boxdescription.TextChanged += new System.EventHandler(this.boxdescription_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Description:";
            // 
            // grouplifetime
            // 
            this.grouplifetime.Controls.Add(this.boxautorenew);
            this.grouplifetime.Controls.Add(this.boxlifetime);
            this.grouplifetime.Controls.Add(this.lifetimespecify);
            this.grouplifetime.Controls.Add(this.lifetimeinfinite);
            this.grouplifetime.Location = new System.Drawing.Point(130, 90);
            this.grouplifetime.Name = "grouplifetime";
            this.grouplifetime.Size = new System.Drawing.Size(115, 113);
            this.grouplifetime.TabIndex = 5;
            this.grouplifetime.TabStop = false;
            this.grouplifetime.Text = "Lifetime";
            // 
            // boxautorenew
            // 
            this.boxautorenew.AutoSize = true;
            this.boxautorenew.Checked = true;
            this.boxautorenew.CheckState = System.Windows.Forms.CheckState.Checked;
            this.boxautorenew.Location = new System.Drawing.Point(6, 91);
            this.boxautorenew.Name = "boxautorenew";
            this.boxautorenew.Size = new System.Drawing.Size(85, 17);
            this.boxautorenew.TabIndex = 11;
            this.boxautorenew.Text = "Auto-Renew";
            this.boxautorenew.UseVisualStyleBackColor = true;
            // 
            // boxlifetime
            // 
            this.boxlifetime.Location = new System.Drawing.Point(6, 65);
            this.boxlifetime.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.boxlifetime.Name = "boxlifetime";
            this.boxlifetime.Size = new System.Drawing.Size(105, 20);
            this.boxlifetime.TabIndex = 10;
            this.boxlifetime.ValueChanged += new System.EventHandler(this.boxlifetime_ValueChanged);
            // 
            // lifetimespecify
            // 
            this.lifetimespecify.AutoSize = true;
            this.lifetimespecify.Checked = true;
            this.lifetimespecify.Location = new System.Drawing.Point(6, 42);
            this.lifetimespecify.Name = "lifetimespecify";
            this.lifetimespecify.Size = new System.Drawing.Size(86, 17);
            this.lifetimespecify.TabIndex = 2;
            this.lifetimespecify.TabStop = true;
            this.lifetimespecify.Text = "Specify (sec)";
            this.lifetimespecify.UseVisualStyleBackColor = true;
            this.lifetimespecify.CheckedChanged += new System.EventHandler(this.lifetimespecify_CheckedChanged);
            // 
            // lifetimeinfinite
            // 
            this.lifetimeinfinite.AutoSize = true;
            this.lifetimeinfinite.Location = new System.Drawing.Point(6, 19);
            this.lifetimeinfinite.Name = "lifetimeinfinite";
            this.lifetimeinfinite.Size = new System.Drawing.Size(56, 17);
            this.lifetimeinfinite.TabIndex = 0;
            this.lifetimeinfinite.Text = "Infinite";
            this.lifetimeinfinite.UseVisualStyleBackColor = true;
            this.lifetimeinfinite.CheckedChanged += new System.EventHandler(this.lifetimeinfinite_CheckedChanged);
            // 
            // btnaddmapping
            // 
            this.btnaddmapping.Location = new System.Drawing.Point(9, 262);
            this.btnaddmapping.Name = "btnaddmapping";
            this.btnaddmapping.Size = new System.Drawing.Size(111, 35);
            this.btnaddmapping.TabIndex = 7;
            this.btnaddmapping.Text = "Add Mapping";
            this.btnaddmapping.UseVisualStyleBackColor = true;
            this.btnaddmapping.Click += new System.EventHandler(this.btnaddmapping_Click);
            // 
            // groupprotocol
            // 
            this.groupprotocol.Controls.Add(this.protocolboth);
            this.groupprotocol.Controls.Add(this.protocoludp);
            this.groupprotocol.Controls.Add(this.protocoltcp);
            this.groupprotocol.Location = new System.Drawing.Point(5, 167);
            this.groupprotocol.Name = "groupprotocol";
            this.groupprotocol.Size = new System.Drawing.Size(119, 89);
            this.groupprotocol.TabIndex = 6;
            this.groupprotocol.TabStop = false;
            this.groupprotocol.Text = "Protocol";
            // 
            // protocolboth
            // 
            this.protocolboth.AutoSize = true;
            this.protocolboth.Location = new System.Drawing.Point(6, 65);
            this.protocolboth.Name = "protocolboth";
            this.protocolboth.Size = new System.Drawing.Size(47, 17);
            this.protocolboth.TabIndex = 2;
            this.protocolboth.Text = "Both";
            this.protocolboth.UseVisualStyleBackColor = true;
            // 
            // protocoludp
            // 
            this.protocoludp.AutoSize = true;
            this.protocoludp.Location = new System.Drawing.Point(6, 42);
            this.protocoludp.Name = "protocoludp";
            this.protocoludp.Size = new System.Drawing.Size(45, 17);
            this.protocoludp.TabIndex = 1;
            this.protocoludp.Text = "Udp";
            this.protocoludp.UseVisualStyleBackColor = true;
            // 
            // protocoltcp
            // 
            this.protocoltcp.AutoSize = true;
            this.protocoltcp.Checked = true;
            this.protocoltcp.Location = new System.Drawing.Point(6, 19);
            this.protocoltcp.Name = "protocoltcp";
            this.protocoltcp.Size = new System.Drawing.Size(44, 17);
            this.protocoltcp.TabIndex = 0;
            this.protocoltcp.TabStop = true;
            this.protocoltcp.Text = "Tcp";
            this.protocoltcp.UseVisualStyleBackColor = true;
            // 
            // boxprivateport
            // 
            this.boxprivateport.Location = new System.Drawing.Point(77, 38);
            this.boxprivateport.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.boxprivateport.Name = "boxprivateport";
            this.boxprivateport.Size = new System.Drawing.Size(168, 20);
            this.boxprivateport.TabIndex = 8;
            this.boxprivateport.ValueChanged += new System.EventHandler(this.boxprivateport_ValueChanged);
            // 
            // boxpublicport
            // 
            this.boxpublicport.Location = new System.Drawing.Point(77, 65);
            this.boxpublicport.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.boxpublicport.Name = "boxpublicport";
            this.boxpublicport.Size = new System.Drawing.Size(168, 20);
            this.boxpublicport.TabIndex = 9;
            this.boxpublicport.ValueChanged += new System.EventHandler(this.boxpublicport_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Location = new System.Drawing.Point(130, 209);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(114, 89);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Other";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.Location = new System.Drawing.Point(6, 42);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(77, 17);
            this.checkBox2.TabIndex = 13;
            this.checkBox2.Text = "Keep Alive";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(6, 19);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(75, 17);
            this.checkBox1.TabIndex = 12;
            this.checkBox1.Text = "Auto-Load";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // FormAddSavedMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 302);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.boxpublicport);
            this.Controls.Add(this.boxprivateport);
            this.Controls.Add(this.groupprotocol);
            this.Controls.Add(this.btnaddmapping);
            this.Controls.Add(this.grouplifetime);
            this.Controls.Add(this.boxdescription);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupdevicebinding);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormAddSavedMapping";
            this.Text = "AddMapping";
            this.groupdevicebinding.ResumeLayout(false);
            this.groupdevicebinding.PerformLayout();
            this.grouplifetime.ResumeLayout(false);
            this.grouplifetime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boxlifetime)).EndInit();
            this.groupprotocol.ResumeLayout(false);
            this.groupprotocol.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boxprivateport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxpublicport)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupdevicebinding;
        private System.Windows.Forms.RadioButton portbindall;
        private System.Windows.Forms.RadioButton portbindselected;
        private System.Windows.Forms.TextBox boxdescription;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox grouplifetime;
        private System.Windows.Forms.RadioButton lifetimespecify;
        private System.Windows.Forms.RadioButton lifetimeinfinite;
        private System.Windows.Forms.Button btnaddmapping;
        private System.Windows.Forms.GroupBox groupprotocol;
        private System.Windows.Forms.RadioButton protocolboth;
        private System.Windows.Forms.RadioButton protocoludp;
        private System.Windows.Forms.RadioButton protocoltcp;
        private System.Windows.Forms.NumericUpDown boxprivateport;
        private System.Windows.Forms.NumericUpDown boxpublicport;
        private System.Windows.Forms.NumericUpDown boxlifetime;
        private System.Windows.Forms.CheckBox boxautorenew;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}