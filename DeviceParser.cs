﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portmapper
{
    public class DeviceParser
    {
        public string getEndPoint(string device)
        {
            return device.Split(new[] { "EndPoint: " }, StringSplitOptions.None)[1].Split(new[] { "\n" }, StringSplitOptions.None)[0];
        }

        public string getLastSeen(string device)
        {
            return device.Split(new[] { "Last Seen: " }, StringSplitOptions.None)[1];
        }
        public string getType(string device)
        {
            return device.Split(new[] { "Type: " }, StringSplitOptions.None)[1].Split(new[] { "Last Seen: " }, StringSplitOptions.None)[0];
        }
        public string getControlUrl(string device)
        {
            return device.Split(new[] { "Control Url: " }, StringSplitOptions.None)[1].Split(new[] { " Type: " }, StringSplitOptions.None)[0];
        }
    }
}
