﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace Portmapper
{
    public static class HostLookup
    {
        public static string ReverseIPLookup(IPAddress ipAddress)
        {
            if (ipAddress.AddressFamily != AddressFamily.InterNetwork)
                throw new ArgumentException("IP address is not IPv4.", "ipAddress");
            string domain = string.Join(
                ".", ipAddress.GetAddressBytes().Reverse().Select(b => b.ToString())
                ) + ".in-addr.arpa";
            return DnsGetPtrRecord(domain);
        }

        private static string DnsGetPtrRecord(string domain)
        {
            const short DNS_TYPE_PTR = 0x000C;
            const int DNS_QUERY_STANDARD = 0x00000000;
            const int DNS_ERROR_RCODE_NAME_ERROR = 9003;
            var queryResultSet = IntPtr.Zero;
            try
            {
                int dnsStatus = DnsQuery(
                    domain,
                    DNS_TYPE_PTR,
                    DNS_QUERY_STANDARD,
                    IntPtr.Zero,
                    ref queryResultSet,
                    IntPtr.Zero
                    );
                if (dnsStatus == DNS_ERROR_RCODE_NAME_ERROR)
                    return null;
                if (dnsStatus != 0)
                    throw new Win32Exception(dnsStatus);
                DnsRecordPtr dnsRecordPtr;
                for (var pointer = queryResultSet; pointer != IntPtr.Zero; pointer = dnsRecordPtr.pNext)
                {
                    dnsRecordPtr = (DnsRecordPtr) Marshal.PtrToStructure(pointer, typeof (DnsRecordPtr));
                    if (dnsRecordPtr.wType == DNS_TYPE_PTR)
                        return Marshal.PtrToStringUni(dnsRecordPtr.pNameHost);
                }
                return null;
            }
            finally
            {
                const int DnsFreeRecordList = 1;
                if (queryResultSet != IntPtr.Zero)
                    DnsRecordListFree(queryResultSet, DnsFreeRecordList);
            }
        }

        [DllImport("Dnsapi.dll", EntryPoint = "DnsQuery_W", ExactSpelling = true, CharSet = CharSet.Unicode,
            SetLastError = true)]
        private static extern int DnsQuery(string lpstrName, short wType, int options, IntPtr pExtra,
            ref IntPtr ppQueryResultsSet, IntPtr pReserved);

        [DllImport("Dnsapi.dll", SetLastError = true)]
        private static extern void DnsRecordListFree(IntPtr pRecordList, int freeType);

        [StructLayout(LayoutKind.Sequential)]
        private struct DnsRecordPtr
        {
            public readonly IntPtr pNext;
            public readonly string pName;
            public readonly short wType;
            public readonly short wDataLength;
            public readonly int flags;
            public readonly int dwTtl;
            public readonly int dwReserved;
            public readonly IntPtr pNameHost;
        }
    }
}