﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Portmapper
{
    partial class Listedmapping
    {
        public string desc;
        public int publicport;
        public int index;
        public string id;
        public Listedmapping(string a1, int a2, int a3)
        {
            desc = a1;
            publicport = a2;
            index = a3;
            id = a1 + a2;
        }
        
    }
    public partial class Listedmapping
    {
        public override bool Equals(object obj)
        {
            Listedmapping other = obj as Listedmapping;
            return other != null && id.Equals(other.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}
