﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Open.Nat;

namespace Portmapper
{
    public partial class Form1 : Form
    {
        private readonly List<Mapping> autorenewmappings = new List<Mapping>();
        public CancellationTokenSource canceltoken = new CancellationTokenSource(2000);
        public List<NatDevice> devices = new List<NatDevice>();
        public NatDiscoverer Discoverer = new NatDiscoverer();
        public bool discoverReady = true;
        public DeviceParser dparse = new DeviceParser();
        public string selectedEndpoint;
        private bool startup = true;
        public bool updatingmappings;
        public List<Open.Nat.Mapping> mappings = new List<Open.Nat.Mapping>(); 
        public Form1()
        {
            InitializeComponent();
            //lvMappings.DoubleBuffering(true);
            //lvSavedMappings.DoubleBuffering(true);
        }

        public async void update()
        {
            lvDevices.Items.Clear();
            // ReSharper disable once ForCanBeConvertedToForeach
            for (int i = 0; i < devices.Count; i++)
            {
                int i1 = i;
                string devicename = dparse.getEndPoint(devices[i].ToString());
                lvDevices.Items.Add(devicename);
                lvDevices.Items[lvDevices.Items.Count - 1].SubItems.Add("");
                lvDevices.Items[lvDevices.Items.Count - 1].SubItems.Add("");
                lvDevices.Items[lvDevices.Items.Count - 1].SubItems.Add("");
                lvDevices.Items[lvDevices.Items.Count - 1].SubItems.Add("");
                lvDevices.Items[lvDevices.Items.Count - 1].SubItems.Add("");
                lvDevices.Items[lvDevices.Items.Count - 1].SubItems.Add("");
                new Thread(async () =>{
                    string t = (await devices[i1].GetExternalIPAsync()).ToString();
                    Invoke(new Action(() =>
                    {lvDevices.Items[lvDevices.Items.Count - 1].SubItems[1].Text = (t);
                    }));}).Start();
                new Thread(() =>
                {
                    string t = GetLocalIPAddress();
                    Invoke(new Action(() =>
                    {
                        lvDevices.Items[lvDevices.Items.Count - 1].SubItems[2].Text = (t);
                    }));
                }).Start();
                new Thread(async () =>
                {
                    string t = (await devices[i1].GetAllMappingsAsync()).Count().ToString();
                    Invoke(new Action(() =>
                    {
                        lvDevices.Items[lvDevices.Items.Count - 1].SubItems[3].Text = (t);
                    }));
                }).Start();

                lvDevices.Items[lvDevices.Items.Count - 1].SubItems[4].Text = (dparse.getType(devices[i].ToString()));
                lvDevices.Items[lvDevices.Items.Count - 1].SubItems[5].Text = (dparse.getLastSeen(devices[i].ToString()));
                lvDevices.Items[lvDevices.Items.Count - 1].SubItems[6].Text = ("");
            }
            if (lvDevices.Items.Count == 1)
                lvDevices.Items[0].Selected = true;
            else if (lvDevices.Items.Count > 0)
                selectedEndpoint = dparse.getEndPoint(devices[lvDevices.SelectedIndices[0]].ToString());
        }

        public static string GetLocalIPAddress()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork))
                return ip.ToString();
            return "";
        }

        private void lvDevices_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedEndpoint = dparse.getEndPoint(devices[lvDevices.SelectedIndices[0]].ToString());
            displayMappings(devices[endpointtodevice(selectedEndpoint)]);
            for (int i = 0; i < lvDevices.Items.Count; i++)
                lvDevices.Items[i].SubItems[6].Text = "";
            lvDevices.Items[lvDevices.SelectedIndices[0]].SubItems[6].Text = "Selected";
        }

        public async void displayMappings(NatDevice device)
        {
            updatingmappings = true;
            lvMappings.Items.Clear();
            foreach (var m in await device.GetAllMappingsAsync())
            {
                lvMappings.Items.Add(m.Description);
                lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(m.PrivatePort.ToString());
                lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(m.PublicPort.ToString());
                lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(m.Protocol.ToString());
                lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(m.PrivateIP.ToString());
                lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(Dns.GetHostEntry(m.PrivateIP).HostName);
                lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(m.Lifetime.ToString());
            }
            removeduplicates();
            updatingmappings = false;
            startup = false;
        }

        public int endpointtodevice(string endpoint)
        {
            return devices.Where(device => dparse.getEndPoint(device.ToString()) == endpoint).Select(device => devices.IndexOf(device)).FirstOrDefault();
        }

        private void btnRunDiscovery_Click(object sender, EventArgs e)
        {
            discover();
        }

        public async Task DiscoverStartup()
        {
            Thread.Sleep(200);
            Invoke(new Action(discover));
        }

        private async void btnMAdd_Click(object sender, EventArgs e)
        {
            var AddForm = new FormAddMapping();
            var result = AddForm.ShowDialog();
            if (result != DialogResult.OK) return;
            var map = AddForm.returnmapping;
            for (int i = 0; i < lvMappings.Items.Count; i++)
                if (lvMappings.Items[i].SubItems[2].Text == map.publicport.ToString())
                {
                    var messageresult = MessageBox.Show("Port already mapped. Try replace?", "Confirmation",
                        MessageBoxButtons.YesNoCancel);
                    if (messageresult == DialogResult.Yes)
                        try
                        {
                            await devices[endpointtodevice(selectedEndpoint)].DeletePortMapAsync(await porttomapping(map.publicport));
                        }
                        catch{MessageBox.Show("Unsuccessful");}
                    else return;
                }
            addmapping(map);
        }

        public async void autorenew()
        {
            if (updatingmappings)
                return;
            foreach (NatDevice device in devices)
                foreach (Open.Nat.Mapping mapping in await device.GetAllMappingsAsync())
                {
                    Open.Nat.Mapping mapping1 = mapping;
                    Open.Nat.Mapping mapping2 = mapping;
                    foreach (Mapping listedmap in autorenewmappings.Where(listedmap => listedmap.publicport == mapping1.PublicPort).Where(listedmap => mapping2.Lifetime < 60))
                    {
                        await device.DeletePortMapAsync(mapping);
                        addmapping(listedmap);
                    }
                }
        }

        private async void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (var device in devices)
            {
                var maps = await device.GetAllMappingsAsync();
                foreach (var map in maps)
                    device.DeletePortMapAsync(map);
            }
        }

        private void boxautodiscover_CheckedChanged(object sender, EventArgs e)
        {
            boxautodiscovertick.Visible = boxautodiscover.Checked;
            timerAutoDiscover.Enabled = boxautodiscover.Checked;
        }

        public int indexOfDevice(string endpoint)
        {
            return
                devices.Where(device => dparse.getEndPoint(device.ToString()) == endpoint).Select(device => devices.IndexOf(device)).FirstOrDefault();
        }

        public async Task<Open.Nat.Mapping> porttomapping(int port)
        {
            return (await devices[endpointtodevice(selectedEndpoint)].GetAllMappingsAsync()).FirstOrDefault(m => m.PublicPort == port);
        }

        public async Task<Open.Nat.Mapping> porttomapping(int port, IEnumerable<Open.Nat.Mapping> mappings)
        {
            return mappings.FirstOrDefault(m => m.PublicPort == port);
        }

        private void btnMUpdate_Click(object sender, EventArgs e)
        {
            btnMUpdate.Text = "Working";
            btnMUpdate.Enabled = false;
            try{displayMappings(devices[indexOfDevice(selectedEndpoint)]);}
            catch{}
            btnMUpdate.Text = "Update";
            btnMUpdate.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            new Thread(delegate() { DiscoverStartup(); }).Start();
        }

        public async void addmapping(Mapping map)
        {
            try {
                switch (map.lifetime)
                {
                    case "Infinite":
                        switch (map.device)
                        {
                            case "Selected Device":
                                switch (map.protocol)
                                {
                                    case "tcp":
                                        mappings.Add(new Open.Nat.Mapping(Protocol.Tcp, map.privateport, map.publicport, map.description));
                                        await devices[endpointtodevice(selectedEndpoint)].CreatePortMapAsync(
                                            mappings.Last());
                                        break;
                                    case "udp":
                                        mappings.Add(new Open.Nat.Mapping(Protocol.Udp, map.privateport, map.publicport, map.description));
                                        await
                                            devices[endpointtodevice(selectedEndpoint)].CreatePortMapAsync(
                                                mappings.Last());
                                        break;
                                    case "both":
                                        mappings.Add(new Open.Nat.Mapping(Protocol.Tcp, map.privateport, map.publicport, map.description));
                                        await
                                            devices[endpointtodevice(selectedEndpoint)].CreatePortMapAsync(
                                                mappings.Last());
                                        mappings.Add(new Open.Nat.Mapping(Protocol.Udp, map.privateport, map.publicport, map.description));
                                        await
                                            devices[endpointtodevice(selectedEndpoint)].CreatePortMapAsync(
                                                mappings.Last());
                                        break;
                                }
                                break;
                            case "All Devices":
                                foreach (var device in devices)
                                {
                                    switch (map.protocol)
                                    {
                                        case "tcp":
                                            mappings.Add(new Open.Nat.Mapping(Protocol.Tcp, map.privateport, map.publicport, map.description));
                                            await device.CreatePortMapAsync(mappings.Last());
                                            break;
                                        case "udp":
                                            mappings.Add(new Open.Nat.Mapping(Protocol.Udp, map.privateport,
                                                map.publicport, map.description));
                                            await
                                                device.CreatePortMapAsync(mappings.Last());
                                            break;
                                        case "both":
                                            mappings.Add(new Open.Nat.Mapping(Protocol.Tcp, map.privateport,
                                                map.publicport, map.description));
                                            await
                                                device.CreatePortMapAsync(mappings.Last());
                                            mappings.Add(new Open.Nat.Mapping(Protocol.Udp, map.privateport,
                                                map.publicport, map.description));
                                            await
                                                device.CreatePortMapAsync(mappings.Last());
                                            break;
                                    }
                                }
                                break;
                        }
                        break;
                    case "specify":
                        switch (map.device)
                        {
                            case "Selected Device":
                                switch (map.protocol)
                                {
                                    case "tcp":
                                        mappings.Add(new Open.Nat.Mapping(Protocol.Tcp, map.privateport, map.publicport, map.lifetimeamount,
                                            map.description));
                                        await devices[endpointtodevice(selectedEndpoint)].CreatePortMapAsync(
                                            mappings.Last());
                                        break;
                                    case "udp":
                                        mappings.Add(new Open.Nat.Mapping(Protocol.Udp, map.privateport, map.publicport,
                                            map.lifetimeamount, map.description));
                                        await
                                            devices[endpointtodevice(selectedEndpoint)].CreatePortMapAsync(mappings.Last());
                                        break;
                                    case "both":
                                        mappings.Add(new Open.Nat.Mapping(Protocol.Tcp, map.privateport, map.publicport,
                                            map.lifetimeamount, map.description));
                                        await
                                            devices[endpointtodevice(selectedEndpoint)].CreatePortMapAsync(mappings.Last());
                                        mappings.Add(new Open.Nat.Mapping(Protocol.Udp, map.privateport, map.publicport,
                                            map.lifetimeamount, map.description));
                                        await
                                            devices[endpointtodevice(selectedEndpoint)].CreatePortMapAsync(
                                                new Open.Nat.Mapping(Protocol.Udp, map.privateport, map.publicport,
                                                    map.lifetimeamount, map.description));
                                        break;
                                }
                                break;
                            case "All Devices":
                                foreach (var device in devices)
                                {
                                    switch (map.protocol)
                                    {
                                        case "tcp":
                                            mappings.Add(new Open.Nat.Mapping(Protocol.Tcp, map.privateport, map.publicport,
                                                map.lifetimeamount, map.description));
                                            await device.CreatePortMapAsync(mappings.Last());
                                            break;
                                        case "udp":
                                            mappings.Add(new Open.Nat.Mapping(Protocol.Udp, map.privateport,
                                                map.publicport, map.lifetimeamount, map.description));
                                            await
                                                device.CreatePortMapAsync(mappings.Last());
                                            break;
                                        case "both":
                                            mappings.Add(new Open.Nat.Mapping(Protocol.Tcp, map.privateport,
                                                map.publicport, map.lifetimeamount, map.description));
                                            await
                                                device.CreatePortMapAsync(mappings.Last());
                                            mappings.Add(new Open.Nat.Mapping(Protocol.Udp, map.privateport,
                                                map.publicport, map.lifetimeamount, map.description));
                                            await
                                                device.CreatePortMapAsync(mappings.Last());
                                            break;
                                    }
                                }
                                break;
                        }
                        break;
                    case "autorenew":
                        autorenewmappings.Add(map);
                        switch (map.device)
                        {
                            case "Selected Device":
                                switch (map.protocol)
                                {
                                    case "tcp":
                                        mappings.Add(new Open.Nat.Mapping(Protocol.Tcp, map.privateport, map.publicport, map.lifetimeamount,
                                            map.description));
                                        await devices[endpointtodevice(selectedEndpoint)].CreatePortMapAsync(mappings.Last());
                                        break;
                                    case "udp":
                                        mappings.Add(new Open.Nat.Mapping(Protocol.Udp, map.privateport, map.publicport,
                                            map.lifetimeamount, map.description));
                                        await
                                            devices[endpointtodevice(selectedEndpoint)].CreatePortMapAsync(mappings.Last());
                                        break;
                                    case "both":
                                        mappings.Add(new Open.Nat.Mapping(Protocol.Tcp, map.privateport, map.publicport,
                                            map.lifetimeamount, map.description));
                                        await
                                            devices[endpointtodevice(selectedEndpoint)].CreatePortMapAsync(mappings.Last());
                                        mappings.Add(new Open.Nat.Mapping(Protocol.Udp, map.privateport, map.publicport,
                                            map.lifetimeamount, map.description));
                                        await
                                            devices[endpointtodevice(selectedEndpoint)].CreatePortMapAsync(mappings.Last());
                                        break;
                                }
                                break;
                            case "All Devices":
                                foreach (var device in devices)
                                {
                                    switch (map.protocol)
                                    {
                                        case "tcp":
                                            mappings.Add(new Open.Nat.Mapping(Protocol.Tcp, map.privateport, map.publicport,
                                                map.lifetimeamount, map.description));
                                            await device.CreatePortMapAsync(mappings.Last());
                                            break;
                                        case "udp":
                                            mappings.Add(new Open.Nat.Mapping(Protocol.Udp, map.privateport,
                                                map.publicport, map.lifetimeamount, map.description));
                                            await
                                                device.CreatePortMapAsync(mappings.Last());
                                            break;
                                        case "both":
                                            mappings.Add(new Open.Nat.Mapping(Protocol.Tcp, map.privateport,
                                                map.publicport, map.lifetimeamount, map.description));
                                            await
                                                device.CreatePortMapAsync(mappings.Last());
                                            mappings.Add(new Open.Nat.Mapping(Protocol.Udp, map.privateport,
                                                map.publicport, map.lifetimeamount, map.description));
                                            await
                                                device.CreatePortMapAsync(mappings.Last());
                                            break;
                                    }
                                }
                                break;
                        }
                        break;
                }
            }
            catch(Exception e)
            {
                
            }
            lvMappings.Items.Add(map.description);
            lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(map.privateport.ToString());
            lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(map.publicport.ToString());
            lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(FirstLetterToUpper(map.protocol));
            lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(GetLocalIPAddress());
            lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(Dns.GetHostEntry(GetLocalIPAddress()).HostName);
            lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(map.lifetimeamount.ToString());
            string text = lvMappings.Items[lvMappings.Items.Count - 1].SubItems[4].Text;
            var t = lvMappings.Items[lvMappings.Items.Count - 1].SubItems[5].Text;
        }
        public async void discover()
        {
            discoverReady = false;
            btnRunDiscovery.Text = "Discovery Running";
            btnRunDiscovery.Enabled = false;
            IEnumerable<NatDevice> d1 = await Discoverer.DiscoverDevicesAsync(PortMapper.Upnp, canceltoken);
            IEnumerable<NatDevice> d2 = await Discoverer.DiscoverDevicesAsync(PortMapper.Pmp, canceltoken);
            devices.AddRange(d1.ToList());
            devices.AddRange(d2.ToList());
            discoverReady = true;
            btnRunDiscovery.Enabled = true;
            btnRunDiscovery.Text = "Run Discovery";
            update();
        }
        public string FirstLetterToUpper(string str)
        {
            if (str == null)
                return null;
            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);
            return str.ToUpper();
        }
        public void removeduplicates()
        {
            List<Listedmapping> lmaps = new List<Listedmapping>();
            for (int i = 0; i < lvMappings.Items.Count; i++)
            {
                lmaps.Add(new Listedmapping(lvMappings.Items[i].SubItems[0].Text, int.Parse(lvMappings.Items[i].SubItems[2].Text), i));
            }
            lmaps = lmaps.Distinct().ToList();
            foreach (Listedmapping lm in lmaps)
            {
                bool found = false;
                for (int i = 0; i < lvMappings.Items.Count; i++)
                {
                    if (lm.desc != lvMappings.Items[i].SubItems[0].Text || lm.publicport.ToString() != lvMappings.Items[i].SubItems[2].Text) continue;
                    if (!found)
                        found = true;
                    else lvMappings.Items[i].SubItems[0].Text = "il3u4ry3ou4hrw3l4kjbgzw0394u";
                }
            }
            bool done = false;
            while (!done)
            {
                for (int i = 0; i < lvMappings.Items.Count; i++)
                {
                    if (lvMappings.Items[i].SubItems[0].Text == "il3u4ry3ou4hrw3l4kjbgzw0394u")
                        lvMappings.Items[i].Remove();
                }
                done = true;
                for (int i = 0; i < lvMappings.Items.Count; i++)
                {
                    if (lvMappings.Items[i].SubItems[0].Text == "il3u4ry3ou4hrw3l4kjbgzw0394u")
                        done = false;
                }
            }
        }

        private void timerautorenew_Tick(object sender, EventArgs e)
        {
            autorenew();
        }

        private async void lifetimerefresh_Tick(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized || updatingmappings || startup) return;
            foreach (var m in await devices[endpointtodevice(selectedEndpoint)].GetAllMappingsAsync())
            {
                bool found = false;
                for (int i = 0; i < lvMappings.Items.Count; i++)
                {
                    if (lvMappings.Items[i].SubItems[2].Text != m.PublicPort.ToString()) continue;
                    lvMappings.Items[i].SubItems[6].Text = m.Lifetime.ToString();
                    found = true;
                }
                if (found) continue;
                lvMappings.Items.Add(m.Description);
                lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(m.PrivatePort.ToString());
                lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(m.PublicPort.ToString());
                lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(m.Protocol.ToString());
                lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(m.PrivateIP.ToString());
                lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(Dns.GetHostEntry(m.PrivateIP).HostName);
                lvMappings.Items[lvMappings.Items.Count - 1].SubItems.Add(m.Lifetime.ToString());
                string text = lvMappings.Items[lvMappings.Items.Count - 1].SubItems[4].Text;
                string temptext = lvMappings.Items[lvMappings.Items.Count - 1].SubItems[5].Text;
            }
            var mapstemp = await devices[endpointtodevice(selectedEndpoint)].GetAllMappingsAsync();
            var maps = mapstemp as IList<Open.Nat.Mapping> ?? mapstemp.ToList();
            for (int i = 0; i < lvMappings.Items.Count; i++)
            {
                if (maps.All(m => lvMappings.Items[i].SubItems[2].Text != m.PublicPort.ToString()))
                {
                    lvMappings.Items[i].Remove();
                }
            }
            removeduplicates();
        }

        private async void btnMRemove_Click(object sender, EventArgs e)
        {
            btnMRemove.Text = "Working";
            btnMRemove.Enabled = false;
            try
            {
                devices[endpointtodevice(selectedEndpoint)].DeletePortMapAsync(await porttomapping(int.Parse(lvMappings.Items[lvMappings.SelectedIndices[0]].SubItems[2].Text)));
            }
            catch{}
            var mapstemp = await devices[endpointtodevice(selectedEndpoint)].GetAllMappingsAsync();
            var maps = mapstemp as IList<Open.Nat.Mapping> ?? mapstemp.ToList();
            for (int i = 0; i < lvMappings.Items.Count; i++)
            {
                if (maps.Any(m => lvMappings.Items[i].SubItems[2].Text == m.PublicPort.ToString())) continue;
                lvMappings.Items[i].Remove();
                i = lvMappings.Items.Count;
            }
            btnMRemove.Text = "Remove";
            btnMRemove.Enabled = true;
            update();
        }

        private void btnMRemoveAll_Click(object sender, EventArgs e)
        {
            btnMRemoveAll.Text = "Working";
            btnMRemoveAll.Enabled = false;
            new Thread(async () =>
            {
                Thread.CurrentThread.IsBackground = true;
                var mapstemp = await devices[endpointtodevice(selectedEndpoint)].GetAllMappingsAsync();
                var enumerable = mapstemp as Open.Nat.Mapping[] ?? mapstemp.ToArray();
                for (int i = 0; i < lvMappings.Items.Count; i++)
                {
                    try
                    {
                        await devices[endpointtodevice(selectedEndpoint)].DeletePortMapAsync(await porttomapping(Convert.ToInt32(lvMappings.Items[i].SubItems[2].Text), enumerable));
                    }
                    catch { }
                }
                var maps = mapstemp as IList<Open.Nat.Mapping> ?? enumerable.ToList();
                for (int i = 0; i < lvMappings.Items.Count; i++)
                {
                    string texttemp = "";
                    Invoke(new Action(() =>
                    {
                        texttemp = lvMappings.Items[i].SubItems[2].Text;
                    }));
                    if (maps.Any(m => texttemp == m.PublicPort.ToString())) continue;
                    lvMappings.Items[i].Remove();
                    i = lvMappings.Items.Count;
                }
                Invoke(new Action(() =>
                {
                    btnMRemoveAll.Text = "Remove All";
                    btnMRemoveAll.Enabled = true;
                }));
            }).Start();
            
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            new Thread(async () =>
            {
                Thread.CurrentThread.IsBackground = true;
                var mapstemp = await devices[endpointtodevice(selectedEndpoint)].GetAllMappingsAsync();
                for (int i = 0; i < lvMappings.Items.Count; i++)
                {
                    try
                    {
                        await devices[endpointtodevice(selectedEndpoint)].DeletePortMapAsync(await porttomapping(Convert.ToInt32(lvMappings.Items[i].SubItems[2].Text), mapstemp));
                    }
                    catch { }
                }
                IList<Open.Nat.Mapping> maps = mapstemp as IList<Open.Nat.Mapping> ?? mapstemp.ToList();
                for (int i = 0; i < lvMappings.Items.Count; i++)
                {
                    string texttemp = "";
                    Invoke(new Action(() =>
                    {
                        texttemp = lvMappings.Items[i].SubItems[2].Text;
                    }));
                    if (maps.Any(m => texttemp == m.PublicPort.ToString())) continue;
                    lvMappings.Items[i].Remove();
                    i = lvMappings.Items.Count;
                }
            }).Start();
            Application.Exit();
        }

        private void timerAutoDiscover_Tick(object sender, EventArgs e)
        {
            discover();
        }
    }
}