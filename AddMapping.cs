﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Open.Nat;
namespace Portmapper
{
    public partial class FormAddMapping : Form
    {
        public FormAddMapping()
        {
            InitializeComponent();
            boxlifetime.Text = "180";
        }
        public Mapping returnmapping { get; set; }
        string device;
        string Protocol;
        int privatePort;
        int publicPort;
        int lifetime; 
        string description;
        private void btnaddmapping_Click(object sender, EventArgs e)
        {
            string type_device = "";
            string type_lifetime = "";
            string type_protocol = "";
            if (portbindselected.Checked)
                type_device = "Selected Device";
            else if (portbindall.Checked)
                type_device = "All Devices";
            if (lifetimeinfinite.Checked)
            {
                type_lifetime = "Infinite";
                lifetime = 0;
            }
            else if (lifetimespecify.Checked && !boxautorenew.Checked)
                type_lifetime = "Specify";
            else if (lifetimespecify.Checked && boxautorenew.Checked)
                type_lifetime = "autorenew";
            if (boxautorenew.Checked && boxautorenew.Enabled)
                type_lifetime = "autorenew";
            else if (type_lifetime == "Specify (sec)")
                type_lifetime = "specify";
            if (protocoltcp.Checked)
                type_protocol = "tcp";
            else if (protocoludp.Checked)
                type_protocol = "udp";
            else if (protocolboth.Checked)
                type_protocol = "both";
            returnmapping = new Mapping(type_device, type_protocol, privatePort, publicPort, type_lifetime, lifetime, description);
            DialogResult = DialogResult.OK;
            Close();
        }

        private void boxdescription_TextChanged(object sender, EventArgs e)
        {
            description = boxdescription.Text;
        }

        private void boxprivateport_ValueChanged(object sender, EventArgs e)
        {
            privatePort = int.Parse(boxprivateport.Text);
        }

        private void boxpublicport_ValueChanged(object sender, EventArgs e)
        {
            publicPort = int.Parse(boxpublicport.Text);
        }

        private void lifetimeinfinite_CheckedChanged(object sender, EventArgs e)
        {
            boxlifetime.Enabled = false;
            boxautorenew.Enabled = false;
        }

        private void lifetimespecify_CheckedChanged(object sender, EventArgs e)
        {
            boxlifetime.Enabled = true;
        }

        private void boxlifetime_ValueChanged(object sender, EventArgs e)
        {
            lifetime = int.Parse(boxlifetime.Text);
        }

        private void FormAddMapping_Load(object sender, EventArgs e)
        {
            boxlifetime.Text = "300";
        }
    }
}
