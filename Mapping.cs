﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portmapper
{
    public class Mapping
    {
        public string device;
        public string protocol;
        public int privateport;
        public int publicport;
        public string lifetime;
        public int lifetimeamount;
        public string description;

        public Mapping(string a1, string a2, int a3, int a4, string a5, int a6, string a7)
        {
            device = a1;
            protocol = a2;
            privateport = a3;
            publicport = a4;
            lifetime = a5;
            lifetimeamount = a6; 
            description = a7;
        }
    }
}
