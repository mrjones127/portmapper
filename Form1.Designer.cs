﻿namespace Portmapper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lvDevices = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lvMappings = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lvSavedMappings = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnSNModify = new System.Windows.Forms.Button();
            this.btnMUpdate = new System.Windows.Forms.Button();
            this.btnMRemoveAll = new System.Windows.Forms.Button();
            this.btnSMRemoveSelected = new System.Windows.Forms.Button();
            this.btnMRemove = new System.Windows.Forms.Button();
            this.btnMAdd = new System.Windows.Forms.Button();
            this.btnSMRemoveAll = new System.Windows.Forms.Button();
            this.btnSMLoadAll = new System.Windows.Forms.Button();
            this.btnSMLoad = new System.Windows.Forms.Button();
            this.btnSMAdd = new System.Windows.Forms.Button();
            this.btnInfo = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.boxautodiscovertick = new System.Windows.Forms.NumericUpDown();
            this.boxautodiscover = new System.Windows.Forms.CheckBox();
            this.btnRunDiscovery = new System.Windows.Forms.Button();
            this.timerautorenew = new System.Windows.Forms.Timer(this.components);
            this.lifetimerefresh = new System.Windows.Forms.Timer(this.components);
            this.timerAutoDiscover = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boxautodiscovertick)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lvDevices);
            this.groupBox1.Location = new System.Drawing.Point(1, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1308, 97);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Devices";
            // 
            // lvDevices
            // 
            this.lvDevices.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader16,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader15,
            this.columnHeader19});
            this.lvDevices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvDevices.GridLines = true;
            this.lvDevices.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvDevices.Location = new System.Drawing.Point(3, 16);
            this.lvDevices.MultiSelect = false;
            this.lvDevices.Name = "lvDevices";
            this.lvDevices.Size = new System.Drawing.Size(1302, 78);
            this.lvDevices.TabIndex = 1;
            this.lvDevices.UseCompatibleStateImageBehavior = false;
            this.lvDevices.View = System.Windows.Forms.View.Details;
            this.lvDevices.SelectedIndexChanged += new System.EventHandler(this.lvDevices_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "EndPoint-Address";
            this.columnHeader1.Width = 151;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "External-Address";
            this.columnHeader16.Width = 146;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Client-Address";
            this.columnHeader2.Width = 137;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Mappings";
            this.columnHeader3.Width = 95;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Service Type";
            this.columnHeader4.Width = 534;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Discovery Date";
            this.columnHeader15.Width = 130;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Status";
            this.columnHeader19.Width = 105;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lvMappings);
            this.groupBox2.Location = new System.Drawing.Point(1, 133);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(641, 421);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mappings";
            // 
            // lvMappings
            // 
            this.lvMappings.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader18,
            this.columnHeader20,
            this.columnHeader17});
            this.lvMappings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvMappings.GridLines = true;
            this.lvMappings.Location = new System.Drawing.Point(3, 16);
            this.lvMappings.Name = "lvMappings";
            this.lvMappings.Size = new System.Drawing.Size(635, 402);
            this.lvMappings.TabIndex = 4;
            this.lvMappings.UseCompatibleStateImageBehavior = false;
            this.lvMappings.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Description";
            this.columnHeader5.Width = 160;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Local-Port";
            this.columnHeader9.Width = 70;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Public-Port";
            this.columnHeader10.Width = 70;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Protocol";
            this.columnHeader11.Width = 59;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Client-Address";
            this.columnHeader18.Width = 108;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Host";
            this.columnHeader20.Width = 90;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Lifetime (sec)";
            this.columnHeader17.Width = 74;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lvSavedMappings);
            this.groupBox3.Location = new System.Drawing.Point(645, 133);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(664, 421);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Saved Mappings";
            // 
            // lvSavedMappings
            // 
            this.lvSavedMappings.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader21,
            this.columnHeader14});
            this.lvSavedMappings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvSavedMappings.GridLines = true;
            this.lvSavedMappings.Location = new System.Drawing.Point(3, 16);
            this.lvSavedMappings.Name = "lvSavedMappings";
            this.lvSavedMappings.Size = new System.Drawing.Size(658, 402);
            this.lvSavedMappings.TabIndex = 4;
            this.lvSavedMappings.UseCompatibleStateImageBehavior = false;
            this.lvSavedMappings.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Description";
            this.columnHeader6.Width = 153;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Local-Port";
            this.columnHeader7.Width = 67;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Public-Port";
            this.columnHeader8.Width = 68;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Protocol";
            this.columnHeader12.Width = 57;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "EndPoint Binding";
            this.columnHeader13.Width = 97;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Lifetime";
            this.columnHeader21.Width = 134;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Status";
            this.columnHeader14.Width = 76;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnSNModify);
            this.groupBox4.Controls.Add(this.btnMUpdate);
            this.groupBox4.Controls.Add(this.btnMRemoveAll);
            this.groupBox4.Controls.Add(this.btnSMRemoveSelected);
            this.groupBox4.Controls.Add(this.btnMRemove);
            this.groupBox4.Controls.Add(this.btnMAdd);
            this.groupBox4.Controls.Add(this.btnSMRemoveAll);
            this.groupBox4.Controls.Add(this.btnSMLoadAll);
            this.groupBox4.Controls.Add(this.btnSMLoad);
            this.groupBox4.Controls.Add(this.btnSMAdd);
            this.groupBox4.Location = new System.Drawing.Point(1, 550);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1308, 38);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            // 
            // btnSNModify
            // 
            this.btnSNModify.Location = new System.Drawing.Point(1025, 11);
            this.btnSNModify.Name = "btnSNModify";
            this.btnSNModify.Size = new System.Drawing.Size(75, 24);
            this.btnSNModify.TabIndex = 9;
            this.btnSNModify.Text = "Modify";
            this.btnSNModify.UseVisualStyleBackColor = true;
            // 
            // btnMUpdate
            // 
            this.btnMUpdate.Location = new System.Drawing.Point(83, 11);
            this.btnMUpdate.Name = "btnMUpdate";
            this.btnMUpdate.Size = new System.Drawing.Size(75, 24);
            this.btnMUpdate.TabIndex = 3;
            this.btnMUpdate.Text = "Update";
            this.btnMUpdate.UseVisualStyleBackColor = true;
            this.btnMUpdate.Click += new System.EventHandler(this.btnMUpdate_Click);
            // 
            // btnMRemoveAll
            // 
            this.btnMRemoveAll.Location = new System.Drawing.Point(271, 11);
            this.btnMRemoveAll.Name = "btnMRemoveAll";
            this.btnMRemoveAll.Size = new System.Drawing.Size(92, 24);
            this.btnMRemoveAll.TabIndex = 2;
            this.btnMRemoveAll.Text = "Remove All";
            this.btnMRemoveAll.UseVisualStyleBackColor = true;
            this.btnMRemoveAll.Click += new System.EventHandler(this.btnMRemoveAll_Click);
            // 
            // btnSMRemoveSelected
            // 
            this.btnSMRemoveSelected.Location = new System.Drawing.Point(1106, 11);
            this.btnSMRemoveSelected.Name = "btnSMRemoveSelected";
            this.btnSMRemoveSelected.Size = new System.Drawing.Size(112, 24);
            this.btnSMRemoveSelected.TabIndex = 8;
            this.btnSMRemoveSelected.Text = "Remove Selected";
            this.btnSMRemoveSelected.UseVisualStyleBackColor = true;
            // 
            // btnMRemove
            // 
            this.btnMRemove.Location = new System.Drawing.Point(165, 11);
            this.btnMRemove.Name = "btnMRemove";
            this.btnMRemove.Size = new System.Drawing.Size(100, 24);
            this.btnMRemove.TabIndex = 1;
            this.btnMRemove.Text = "Remove Selected";
            this.btnMRemove.UseVisualStyleBackColor = true;
            this.btnMRemove.Click += new System.EventHandler(this.btnMRemove_Click);
            // 
            // btnMAdd
            // 
            this.btnMAdd.Location = new System.Drawing.Point(2, 11);
            this.btnMAdd.Name = "btnMAdd";
            this.btnMAdd.Size = new System.Drawing.Size(75, 24);
            this.btnMAdd.TabIndex = 0;
            this.btnMAdd.Text = "Add";
            this.btnMAdd.UseVisualStyleBackColor = true;
            this.btnMAdd.Click += new System.EventHandler(this.btnMAdd_Click);
            // 
            // btnSMRemoveAll
            // 
            this.btnSMRemoveAll.Location = new System.Drawing.Point(1224, 11);
            this.btnSMRemoveAll.Name = "btnSMRemoveAll";
            this.btnSMRemoveAll.Size = new System.Drawing.Size(81, 24);
            this.btnSMRemoveAll.TabIndex = 7;
            this.btnSMRemoveAll.Text = "Remove All";
            this.btnSMRemoveAll.UseVisualStyleBackColor = true;
            // 
            // btnSMLoadAll
            // 
            this.btnSMLoadAll.Location = new System.Drawing.Point(862, 11);
            this.btnSMLoadAll.Name = "btnSMLoadAll";
            this.btnSMLoadAll.Size = new System.Drawing.Size(76, 24);
            this.btnSMLoadAll.TabIndex = 6;
            this.btnSMLoadAll.Text = "Load All";
            this.btnSMLoadAll.UseVisualStyleBackColor = true;
            // 
            // btnSMLoad
            // 
            this.btnSMLoad.Location = new System.Drawing.Point(780, 11);
            this.btnSMLoad.Name = "btnSMLoad";
            this.btnSMLoad.Size = new System.Drawing.Size(76, 24);
            this.btnSMLoad.TabIndex = 4;
            this.btnSMLoad.Text = "Load";
            this.btnSMLoad.UseVisualStyleBackColor = true;
            // 
            // btnSMAdd
            // 
            this.btnSMAdd.Location = new System.Drawing.Point(944, 11);
            this.btnSMAdd.Name = "btnSMAdd";
            this.btnSMAdd.Size = new System.Drawing.Size(75, 24);
            this.btnSMAdd.TabIndex = 5;
            this.btnSMAdd.Text = "Add";
            this.btnSMAdd.UseVisualStyleBackColor = true;
            // 
            // btnInfo
            // 
            this.btnInfo.Location = new System.Drawing.Point(1137, 11);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(81, 24);
            this.btnInfo.TabIndex = 10;
            this.btnInfo.Text = "Info";
            this.btnInfo.UseVisualStyleBackColor = true;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(1224, 11);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(81, 24);
            this.btnExit.TabIndex = 9;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.boxautodiscovertick);
            this.groupBox7.Controls.Add(this.boxautodiscover);
            this.groupBox7.Controls.Add(this.btnInfo);
            this.groupBox7.Controls.Add(this.btnExit);
            this.groupBox7.Controls.Add(this.btnRunDiscovery);
            this.groupBox7.Location = new System.Drawing.Point(1, 92);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1308, 39);
            this.groupBox7.TabIndex = 11;
            this.groupBox7.TabStop = false;
            // 
            // boxautodiscovertick
            // 
            this.boxautodiscovertick.Location = new System.Drawing.Point(223, 15);
            this.boxautodiscovertick.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.boxautodiscovertick.Name = "boxautodiscovertick";
            this.boxautodiscovertick.Size = new System.Drawing.Size(110, 20);
            this.boxautodiscovertick.TabIndex = 12;
            this.boxautodiscovertick.Visible = false;
            // 
            // boxautodiscover
            // 
            this.boxautodiscover.AutoSize = true;
            this.boxautodiscover.Location = new System.Drawing.Point(124, 16);
            this.boxautodiscover.Name = "boxautodiscover";
            this.boxautodiscover.Size = new System.Drawing.Size(93, 17);
            this.boxautodiscover.TabIndex = 11;
            this.boxautodiscover.Text = "Auto-Discover";
            this.boxautodiscover.UseVisualStyleBackColor = true;
            this.boxautodiscover.CheckedChanged += new System.EventHandler(this.boxautodiscover_CheckedChanged);
            // 
            // btnRunDiscovery
            // 
            this.btnRunDiscovery.Location = new System.Drawing.Point(2, 11);
            this.btnRunDiscovery.Name = "btnRunDiscovery";
            this.btnRunDiscovery.Size = new System.Drawing.Size(107, 24);
            this.btnRunDiscovery.TabIndex = 0;
            this.btnRunDiscovery.Text = "Run Discovery";
            this.btnRunDiscovery.UseVisualStyleBackColor = true;
            this.btnRunDiscovery.Click += new System.EventHandler(this.btnRunDiscovery_Click);
            // 
            // timerautorenew
            // 

            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1310, 590);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox4);
            this.Name = "Form1";
            this.Text = ".NET Port Mapping Tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boxautodiscovertick)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView lvDevices;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ListView lvMappings;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView lvSavedMappings;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnMAdd;
        private System.Windows.Forms.Button btnSMRemoveSelected;
        private System.Windows.Forms.Button btnSMRemoveAll;
        private System.Windows.Forms.Button btnSMLoadAll;
        private System.Windows.Forms.Button btnSMAdd;
        private System.Windows.Forms.Button btnSMLoad;
        private System.Windows.Forms.Button btnMUpdate;
        private System.Windows.Forms.Button btnMRemoveAll;
        private System.Windows.Forms.Button btnMRemove;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btnRunDiscovery;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.Button btnSNModify;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.CheckBox boxautodiscover;
        private System.Windows.Forms.NumericUpDown boxautodiscovertick;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.Timer timerautorenew;
        private System.Windows.Forms.Timer lifetimerefresh;
        private System.Windows.Forms.Timer timerAutoDiscover;
    }
}

